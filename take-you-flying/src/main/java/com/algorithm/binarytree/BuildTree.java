package com.algorithm.binarytree;

import java.util.Iterator;
import java.util.LinkedList;

public class BuildTree {

    public TreeNode searchNode(TreeNode root,int index) {//广度优先搜索,查找父节点
        if(root==null||index<0)return null;
        LinkedList<TreeNode> list = new LinkedList<>();//链表，这里我将其作为队列
        list.add(root);//把数据加入到队列尾部
        while(!list.isEmpty()) {
            TreeNode node = list.poll();
            if(node.treeIndex==index)
                return node;
            if(node.left!=null)
                list.add(node.left);
            if(node.right!=null)
                list.add(node.right);
        }

        return null;
    }


    public TreeNode create(String[] levelOrder) {//考虑到给的数组有null值，故用String类型
        if(levelOrder.length==0)
            return null;
        TreeNode root = new TreeNode(levelOrder[0]);//根节点
        LinkedList<Integer> list = new LinkedList<>();//链表，这里我将其作为队列
        for(int i=1;i<levelOrder.length;i++) {
            if(levelOrder[i]==null||"null".equals(levelOrder[i])) {
                list.add(i);
                continue;
            }
            TreeNode node = new TreeNode(levelOrder[i]);
            node.treeIndex = i;

            LinkedList<Integer> newList = new LinkedList();
            for (Iterator iterator = list.iterator(); iterator.hasNext();) {
                newList.add((Integer) iterator.next());

            }
            buildTree(root,node,i,newList,levelOrder);

        }
        return root;
    }


    //建立树
    public TreeNode buildTree(TreeNode root,TreeNode node,int i,LinkedList<Integer> list,String[] levelOrder) {
        int NULLSUM = compareIndex(list,levelOrder,i);
        /*
         * 如题目给的示例：给定二叉树[5,3,7,9,null,11,2,4,-1, null,null,2 ,-2]
         *                 index： 0,1,2,3,  4 ,5 ,6,7, 8,   9 , 10 ,11,12
         *
         *                 5
         *                / \
         *               3    7
         *              /    / \
         *          9    11  2
         *         / \      / \
         *        4   -1   2  -2
         * 思路:1.定义NULLSUM变量记录null节点个数
         *        2.通过compareIndex函数计算该节点的父节点层及以上出现null节点个数
         *        3.(i-2)/2+NULLSUM可以计算出该节点的父节点
         */
        if(i%2==0) {
            TreeNode parent = searchNode(root,(i-2)/2+NULLSUM);
            while(parent==null) {
                NULLSUM++;
                parent = searchNode(root,(i-2)/2+NULLSUM);
            }
            parent.right = node;
        }else {
            TreeNode parent = searchNode(root,(i-1)/2+NULLSUM);
            while(parent==null) {
                NULLSUM++;
                parent = searchNode(root,(i-1)/2+NULLSUM);
            }
            parent.left = node;
        }

        return root;
    }

    /*
     * 比较下标所指向的值，判断当前节点的父节点下标所在数组位置的值是否等于null
     * list为存储空值的下标队列，从头到尾取值，并计算比较当前节点下标比栈值的子节点大
     * 若大于，这NULLSUM++,否则停止，返回NULLSUM值
     */
    public int compareIndex(LinkedList<Integer> list, String[] order, int i) {
        int sum,NULLSUM = 0;//记录数组中null的数量
        if(list==null&&list.size()==0) {
            return 0;
        }
        while(!list.isEmpty()&&i>list.poll()*2) {
            NULLSUM++;
        }
        return NULLSUM;
    }


}