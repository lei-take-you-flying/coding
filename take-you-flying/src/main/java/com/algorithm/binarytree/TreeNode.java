package com.algorithm.binarytree;

public class TreeNode {
    public int val;
    /*
     * 由于题目给的二叉树中节点值不唯一，
     * 增加treeIndex做唯一标识
     */
    public int treeIndex;//对应数组下标
    public TreeNode left;
    public TreeNode right;
    //由于数组为String类型，需要转型为整型，方便后面运算
    public TreeNode(String x) {
        val = Integer.parseInt(x);
    }

    public TreeNode() {

    }

    @Override
    public String toString() {
        return "TreeNode [val=" + val + "]";
    }

}
