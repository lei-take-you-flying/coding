package com.algorithm.binarytree;

import java.util.LinkedList;
import java.util.List;

public class PathSum {

    /*
     * 传入的参数分别为根节点root、定值K和当前节点所在层数dept(这个非常好用，因为传入的根节点有可能只是树中某节点)
     */
    public List<List<Integer>> pathSumEntry(TreeNode root,int K,int dept){
        List<List<Integer>> result = new LinkedList<List<Integer>>();//用于保存所有匹配路径
        List<Integer> currentResult = new LinkedList<Integer>();//用于保存找到的当前匹配的路径
        pathSum(root,K,currentResult,result,dept);
        return result;
    }
    /*
     * 这里主要使用递归加回溯的思想
     */
    public void pathSum(TreeNode root, int K, List<Integer> currentResult, List<List<Integer>>result, int dept) {
        if(root==null)return;
        currentResult.add(new Integer(root.val));
        if(root.left==null&&root.right==null&&K==root.val+dept) {
            result.add(new LinkedList(currentResult));
        }else {
            pathSum(root.left,K-root.val-dept,currentResult,result,dept+1);
            pathSum(root.right,K-root.val-dept,currentResult,result,dept+1);
        }
        currentResult.remove(currentResult.size()-1);
    }
}
