package com.algorithm.binarytree;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("输入K:");
        int K = input.nextInt();
        System.out.println("输入数组(以'#'结束,例如：5 3 7 9 null 11 2 4 -1 null null 2 -2 #)");
        List<String> list = new ArrayList<String>();//集合接收输入串
        String cin = null;
        while(!"#".equals((cin=input.next()))) {
            list.add(cin);

        }
        //将集合转成字符串数组
        String[] levelOrder = new String[list.size()];
        for(int i=0;i<=list.size()-1;i++) {
            levelOrder[i] = list.get(i);
        }

        //从左到右构造二叉树，并寻找路径和等于K的路径
        BuildTree tree = new BuildTree();
        TreeNode root = tree.create(levelOrder);
        printTree(root,K,0);
    }

    public static void printTree(TreeNode root,int K,int dept) {
        if(root==null)return;
        PathSum pathSum = new PathSum();

        List<List<Integer>> result = pathSum.pathSumEntry(root, K,dept);
        for (List resultList : result) {
            System.out.println(resultList);
        }

        printTree(root.left,K,dept+1);
        printTree(root.right,K,dept+1);
    }
}
